const bull = require("bull");
const { redis } = require("../src/settings");

console.log("ejecutando test...");
async function main() {
    const job = await bull("curso", {
        redis: { host: redis.host, port: redis.port },
    }).add({
        id: 2,
    });

    const result = await job.finished();
    console.log(result);
    if (result.statusCode === 200) {
        console.log("Solicitud Correcta!!");
    }
}
main();
