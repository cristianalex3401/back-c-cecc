const { Services } = require("../Services");
const { internalError } = require("../settings");
const { queueView } = require("./index");

queueView.process(async function (job, done) {
    try {
        const { id } = job.data; // LO QUE NOS VAN A ENVIAR A NOSOTROS...
        console.log(id);
        const { statusCode, data, message } = await Services({ id });
        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapter Adapter", error: error.toString() });
        done(null, { statusCode: 500, data: null, message: internalError });
    }
});
